package com.example.demotestoh.bean;

import java.util.ArrayList;

public class CookingRecord {
    String name;
    String id = "";
    ArrayList<Item> sheet;

    public CookingRecord(String name, String id, ArrayList<Item> sheet) {
        this.name = name;
        this.id = id;
        this.sheet = sheet;
    }

    public CookingRecord() {

        this.sheet = new ArrayList<Item>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void sheetAdd(int heatControl,long time){
        sheet.add(new Item(heatControl,time));
    }

    public ArrayList<Item> getSheet() {
        return sheet;
    }

    public void setSheet(ArrayList<Item> sheet) {
        this.sheet = sheet;
    }
}
