package com.example.demotestoh.bean;

import com.example.demotestoh.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;

import java.util.List;

public class ListData extends BaseItemProvider {


    public List<CookingRecord> list;    //美食数据列表
    public AbilitySlice slice;


    public ListData(List<CookingRecord> list, AbilitySlice slice) {
        this.list = list;
        this.slice = slice;
    }

    public class SettingHolder {
        Text settingtext;
        Button settingbutton1;
        Button settingbutton2;
        SettingHolder(Component component) {
            settingtext = (Text) component.findComponentById(ResourceTable.Id_item_index);
            settingbutton1 = (Button) component.findComponentById(ResourceTable.Id_itembutton1);
            settingbutton2 = (Button) component.findComponentById(ResourceTable.Id_itembutton2);
        }
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        if (list != null && i >= 0 && i < list.size()){
            return list.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        SettingHolder holder;
        CookingRecord cookingRecord = list.get(i);
        if (component == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_sample, null, false);
            holder = new SettingHolder(cpt);
            cpt.setTag(holder);
        } else {
            cpt = component;
            holder = (SettingHolder)cpt.getTag();
        }

        holder.settingtext.setText(i+": "+cookingRecord.getName()+" "+cookingRecord.getId());

        holder.settingbutton1.setClickedListener(new Component.ClickedListener() {
            //分享美食火候点击事件
            @Override
            public void onClick(Component component) {
                CommonDialog dialog = new CommonDialog(slice);
                Component container = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_share, null, false);
                ((Button) container.findComponentById(ResourceTable.Id_buttonok)).setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        try {
                            cookingRecord.setId(((TextField)container.findComponentById(ResourceTable.Id_text_field)).getText());
                            OkHttps okHttps = new OkHttps();
                            okHttps.httpposts(cookingRecord);
                        }catch (Exception e)
                        {
                            new ToastDialog(slice)
                                    .setText("请输入数字类型！")
                                    // Toast显示在界面中间
                                    .setAlignment(LayoutAlignment.CENTER)
                                    .show();
                        }
                        new ToastDialog(slice)
                                .setText("正在发送！")
                                // Toast显示在界面中间
                                .setAlignment(LayoutAlignment.CENTER)
                                .show();
                        dialog.destroy();
                    }
                });
                ((Button) container.findComponentById(ResourceTable.Id_buttonoff)).setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        dialog.destroy();
                    }
                });
                dialog.setContentCustomComponent(container);
                dialog.setAlignment(LayoutAlignment.CENTER);
                dialog.setSize(800, 700);
                dialog.setCornerRadius(100);
                dialog.show();
            }
        });
        //删除美食火候
        holder.settingbutton2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                list.remove(i);
                notifyDataChanged();
                new ToastDialog(slice)
                        .setText("已删除："+ i)
                        // Toast显示在界面中间
                        .setAlignment(LayoutAlignment.CENTER)
                        .show();

            }
        });
        return cpt;
    }
}
