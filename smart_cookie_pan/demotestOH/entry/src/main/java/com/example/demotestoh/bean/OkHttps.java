package com.example.demotestoh.bean;

import com.google.gson.Gson;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import okhttp3.*;

import java.io.IOException;

public class OkHttps {
    private static final HiLogLabel hilog = new HiLogLabel(HiLog.DEBUG ,0x0000, "test0002");
    //分享火候
    public static void httpposts(CookingRecord cookingRecord)
    {
        Gson gson = new Gson();
        MediaType mediaType = MediaType.parse("text/x-markdown; charset=utf-8");
        String requestBody = gson.toJson(cookingRecord);
        Request request = new Request.Builder()
                .url("http://192.168.0.36:5000/CookingRecord_add")
                .post(RequestBody.create(mediaType, requestBody))
                .build();
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                HiLog.debug(hilog, "onFailure: " + e.getMessage());
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                HiLog.debug(hilog, "onResponse: " + response.body().string());
            }
        });
    }
}
