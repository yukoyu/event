package com.example.demotestoh.bean;

public class Item {
    int heatControl;
    long time;

    public Item(int heatControl, long time) {
        this.heatControl = heatControl;
        this.time = time;
    }

    public int getHeatControl() {
        return heatControl;
    }

    public void setHeatControl(int heatControl) {
        this.heatControl = heatControl;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
