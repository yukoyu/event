package com.example.demotestoh.bean;

import com.example.demotestoh.slice.MainAbilitySlice;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class SockerTest extends Thread{

    private static final HiLogLabel hilog = new HiLogLabel(HiLog.DEBUG ,0x0000, "test0002");
    Socket socket = null;
    InputStream inputStream = null;
    OutputStream outputStream=null;
    public boolean flag;


    MainAbilitySlice mains = null;

    public SockerTest(MainAbilitySlice mains) {
        this.mains = mains;
    }

    public void sockerSend(int a,int b,int c){
        if(socket != null && outputStream != null)
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        byte[] data =  new byte[4];
                        data[0] = (byte) 0xaa;
                        data[1] = (byte) a;
                        data[2] = (byte) b;
                        data[3] = (byte) c;
                        outputStream.write(data);
                        outputStream.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }else{

        }

    }

    @Override
    public void run() {
        try {
            flag = true;
            while (flag)
            {
                socket = new Socket("192.168.0.244", 18000);
                outputStream = socket.getOutputStream();
                inputStream =socket.getInputStream();
                init1();
                socket.close();
            }
        } catch (IOException e) {
            System.out.println("net erorr!"+e);
        }
    }

    public void init1() {

        try {
            byte[] b = new byte[1024];
            while (flag)
            {
                int n = inputStream.read(b);
                if(n !=-1)
                {
                    byte[] data = new byte[n];
                    for (int i=0;i<n;i++)
                    {
                        data[i] = b[i];
                    }
                    mains.sockerCallback(data);
                }else break;
            }
        }catch (Exception e){
            HiLog.debug(hilog,"net erorr!"+e,"");
        }
    }

}
