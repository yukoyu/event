# electricity cake clang



## 一、概述

本Demo是基于Hi3516开发板、鸿蒙手机，使用开源OpenHarmony开发的应用。通过鸿蒙APP可以控制同一局域网内的智能电饼铛设备。

#### 本项目获得了openharmony开源开发者成长计划优秀项目奖

openharmony project 智能电饼铛，基于OpenHarmony开发一款家用智能电饼铛，功能如下： 

1），能够模仿电饼铛功能 

2），能够借助智能手机实现屏幕操作电饼铛 

3），能够及时通知烹饪结果到智能终端

4），智能电饼铛火候录制与分享

5），智能菜单、自主烙烤、加长定时功能,满足烙考煎炸各类烹饪需求

6），多档火力调节,满足不同烘焙需求



## 二、目录介绍

/openharmony/electricitycakeclangdemo							智能电饼铛UI，采用eTS开发方式

/openharmony/native_module_netanddev						  网络通信socket，采用NAIP方式

/demotestOH																			 智能电饼铛手机端代码，采用java方式开发鸿蒙app

/flaskProject																			   服务器后端代码，采用FLask框架



## 三、软件架构
![输入图片说明](media/image-20220220164916839.png)



#### 本项目为openharmony物联网的雏形，为openharmony系统提供先行者作用。



1）.本demo架构，一共分为三个端，智能电饼铛端（eTS+NAPI方式开发）、鸿蒙APP端（harmonyOS java方式开发）、服务器后端（flask框架方式开发）

2）.智能电饼铛端与鸿蒙APP端通讯方式是通过socket TCP

3）.鸿蒙APP端与服务器后端交互通过post请求



## 四、展示成果



![image1](media/image1.png)



## 五、快速开始

### 1).开发板编译环境准备

编译环境搭建包含如下几步：

1. 安装的库和工具
2. 安装python3
3. 安装arm-none-eabi-gcc
4. 获取源码&文件拷贝和修改
5. 编译流程
6. 设备配网

#### 1. 安装的库和工具

使用如下apt-get命令安装下面的库和工具，Ubuntu20.04 64位系统需要安装以下依赖：

安装编译依赖基础软件

```
sudo apt-get install -y build-essential gcc g++ make zlib* libffi-dev git git-lfs
```

#### 2. 安装和配置Python

1. 打开Linux终端。

2. 输入如下命令，查看python版本号，需要使用python3.7以上版本,否则参考 [系统基础环境搭建](https://gitee.com/openharmony/docs/blob/OpenHarmony_1.0.1_release/zh-cn/device-dev/quick-start/搭建系统基础环境.md)。

   ```
   python3 --version
   ```

3. 安装并升级Python包管理工具（pip3）。

```
sudo apt-get install python3-setuptools python3-pip -y
sudo pip3 install --upgrade pip
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests
```

4.安装python模块

```
sudo pip3 install setuptools kconfiglib pycryptodome ecdsa six --upgrade --ignore-installed six
```

#### 3. 安装arm-none-eabi-gcc

1. 打开Linux编译服务器终端。

2. 下载arm-none-eabi-gcc 编译工具,[下载链接地址](https://gitee.com/link?target=https%3A%2F%2Fdeveloper.arm.com%2F-%2Fmedia%2FFiles%2Fdownloads%2Fgnu-rm%2F10.3-2021.10%2Fgcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2)。

3. 解压 gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2安装包至~/toolchain/路径下。

   ```
   mkdir -p ~/toolchain/
   tar -jxvf gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2 -C ~/toolchain/
   ```

4. 设置环境变量。

   ```
   vim ~/.bashrc
   ```

   将以下命令拷贝到.bashrc文件的最后一行，保存并退出。

   ```
   export PATH=~/toolchain/gcc-arm-none-eabi-10.3-2021.10/bin:$PATH
   ```

5. 生效环境变量。

   ```
   source ~/.bashrc
   ```

#### 4. 获取源码&文件拷贝和修改

##### 1.码云工具下载

```
1）下载repo工具
mkdir ~/bin
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > ~/bin/repo
chmod a+x ~/bin/repo

2）设置环境变量并生效
vim ~/.bashrc
export PATH=~/bin:$PATH
source ~/.bashrc 
```

##### 2.代码下载



OpenHarmony代码下载

```
#特别注意：请下载OpenHarmony 3.1 Beta版本
mkdir ~/OpenHarmony_3.1_Beta
cd ~/OpenHarmony_3.1_Beta
repo init -u git@gitee.com:openharmony/manifest.git -b OpenHarmony-3.1-Beta --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

#### 5.编译NAPI

##### 1.将下载的本项目代码/openharmony/native_module_netanddev目录拷贝到foundation/ace/napi/sample/

```
cp /openharmony/native_module_netanddev ~/openharmony3.1/foundation/ace/napi/sample/
```

##### 2.编译

```
./build.sh --product-name Hi3516DV300 --build-target make_test

生成为out/ohos-arm-release/ace/napi/netanddev.z.so
```

#### 6 拷贝动态库

生成的.z.so动态库已经拷贝到PC上

PC串口控制台：

```
mount -o remount,rw /
```

PC命令窗口cmd:

```
hdc_std file send E:\libled.z.so /system/lib/module/
```

PC串口控制台：

```
chmod 666 /system/lib/modulenetanddev.z.so
```



### 2).eTS开发准备

####  1.标准设备环境准备

润和HiSpark Taurus AI Camera(Hi3516d)开发板套件:

- [Hi3516DV300开发板标准设备HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/hi3516_dv300_helloworld/README.md)，参考环境准备、编译和烧录章节;

#### 2.应用编译环境准备

- 下载DevEco Studio [下载地址](https://gitee.com/link?target=https%3A%2F%2Fdeveloper.harmonyos.com%2Fcn%2Fdevelop%2Fdeveco-studio%23download_beta)；
- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md);
- DevEco Studio 点击File -> Open 导入本下面的代码工程electricity-cake-clang/openharmony/electricitycakeclangdemo;

#### 3.项目下载和导入

1）git下载

```
git clone https://gitee.com/yukoyu/electricity-cake-clang.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Entertainment/electricity-cake-clang/openharmony/electricitycakeclangdemo

#### 4.安装应用

- [配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

- 安装应用

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```



### 3).harmonyOS应用开发准备

#### 1.harmonyOS手机设备环境准备

- 准备harmonyOS系统手机

#### 2.应用编译环境准备

- 下载DevEco Studio [下载地址](https://gitee.com/link?target=https%3A%2F%2Fdeveloper.harmonyos.com%2Fcn%2Fdevelop%2Fdeveco-studio%23download_beta)；
- 配置SDK
- DevEco Studio 点击File -> Open 导入本下面的代码工程/demotestOH;

#### 3.项目下载和导入

1）git下载

```
git clone https://gitee.com/yukoyu/electricity-cake-clang.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Entertainment/electricity-cake-clang/demotestOH

#### 4.安装应用

- [配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

- 安装应用

  点击run按钮，进行安装

  

### 4).Flask环境准备

#### 1.下载PyCharm

PyCharm 的下载地址：http://www.jetbrains.com/pycharm/download/#section=windows

#### 2.安装PyCharm

​	安装PyCharm，参考 [PyCharm 安装教程（Windows）](https://www.runoob.com/w3cnote/pycharm-windows-install.html);

#### 3.项目下载和导入

1）git下载

```
git clone https://gitee.com/yukoyu/electricity-cake-clang.git
```

2）项目导入

```
打开PyCharm,点击File->Open->下载路径/FA/Entertainment/electricity-cake-clang/flaskProject
```

#### 4.运行flask后端

```
点击run按钮
```



## 五、帮助



有任何问题可联系：[ggvyyy@163.com](mailto:ggvyyy@163.com)

